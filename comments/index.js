const express = require('express');
const bodyParser = require('body-parser');
const randomBytes = require('crypto');
const cors = require('cors');
const axios = require('axios');

const port = 4001;

const app = express();
app.use(bodyParser.json())
app.use(cors())

const commentsByPostID = {};

function getRandomInt() {
    min = Math.ceil(0);
    max = Math.floor(1000000);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

app.get('/posts/:id/comments',(req,res)=>{
    res.send(commentsByPostID[req.params.id]||[]);
});

app.post('/posts/:id/comments',async (req,res)=>{
    const commentID = getRandomInt().toString()
    const {content} = req.body;

    const comments = commentsByPostID[req.params.id] || [];

    comments.push({id: commentID, content, status: 'pending'});

    commentsByPostID[req.params.id] = comments;

    await axios.post("http://localhost:4005/events",{
        type: 'CommentCreated',
        data:{
            id: commentID,
            content,
            postID: req.params.id,
            status: 'pending'
        }
    })

    res.status(201).send(comments);
});

app.post('/events', async (req, res)=>{
    const received_event_type = req.body.type;

    if(received_event_type === "CommentModerated"){
        const {postID, id, status} = req.body.data;

        const comments = commentsByPostID[postID];

        const comment = comments.find(comment => {
            return id === comment.id;
        });

        comment.status = status;

        await axios.post("http://localhost:4005/events",{
            type: "CommentUpdated",
            data: {
                id,
                status,
                postID,
                content: comment.content
            }
        })
    }

    res.send({})
})

app.listen(port, ()=>{
    console.log("Listening on port ",port);
})
