const express = require('express');
const bodyParser = require('body-parser')
const cors = require('cors');
const axios = require('axios');

const app = express()
app.use(bodyParser.json());
app.use(cors( ))

const posts = {};

function getRandomInt() {
    min = Math.ceil(0);
    max = Math.floor(1000000);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}

app.get('/posts',(req,res)=>{
    res.send(posts);
});

app.post('/posts', async (req,res)=>{
    const id = getRandomInt().toString();
    const {title} = req.body;

    posts[id] = {
        id,title
    };

    await axios.post("http://localhost:4005/events",{
        type: "PostCreated",
        data: {
            id, title
        }
    })

    res.status(201).send(posts[id]);
});

app.post('/events', (req, res)=>{
    const received_event_type = req.body.type;
    console.log("Received an event type: ",received_event_type);
    console.log("Recevied event data: ",req.body.data);

    res.send({});
})

app.listen(4000,()=>{
    console.log('Listening on 4000');
});