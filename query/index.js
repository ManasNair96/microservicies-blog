const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const axios = require('axios');


const app = express();
app.use(bodyParser.json());
app.use(cors());

const port = 4002;

const posts = {}

app.get('/posts', (req,res)=>{
    res.send(posts)
});

app.post('/events', (req,res) =>{
    const {type, data} =  req.body;
    console.log("Type:",type);
    console.log("Data:",data);

    handleEvent(type,data);
    
    res.send({});
});

const handleEvent = (type, data) => {
    if(type === 'PostCreated'){
        const {id, title} = data;

        posts [id] = {id, title, comments: []};
    }

    if(type === 'CommentCreated'){
        let {id, content, postID, status} = data;

        console.log("Comment ID:", id);
        console.log("Content:",content);
        console.log("postID:", postID);

        postID = postID.toString();

        const post = posts[postID];
        console.log(posts);

        post.comments.push({id, content, status});   
    }

    if(type === "CommentUpdated"){
        const {id, content, postID, status} = data;

        const post = posts[postID];
        const comment = post.comments.find(comment =>{
            return comment.id === id;
        });

        comment.status = status;
        comment.content = content;
    }
}

app.listen(port, async ()=>{
    console.log("Listening on port",port);

    const res = await axios.get('http://localhost:5000/events')

    for(let event of res.data){
       console.log("Processing event:",event)
       handleEvent(event.type, event.data);
    }
})